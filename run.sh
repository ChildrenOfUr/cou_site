#! /bin/bash

# Exit at first error
set -e

# Prerequisites:
# - sudo apt install tmux
# - sudo apt install python3 python3-pip pipenv

echo "Installing dependencies..."
pipenv install
echo "...done installing dependencies."

echo "Killing old server process (if any)..."
tmux kill-session -t "cou_site" || true
echo "...done killing old server process (if any)."

echo "Starting server..."
pipenv run tmuxp load -d tmuxp-session.yaml
echo "...done starting server."
