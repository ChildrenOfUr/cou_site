import json

from flask import Blueprint, request, session
from sqlalchemy.orm.exc import NoResultFound

from cou_site.controllers.api import respond, require_auth, remove_script_tags
from cou_site.controllers.api.watching import notify_watchers
from cou_site.models import db
from cou_site.models.category import Category
from cou_site.models.post import Post
from cou_site.models.reply import Reply
from cou_site.models.user import User

submissions = Blueprint("submissions", __name__, url_prefix="/api/submit")


@submissions.route("/post", methods=["POST"])
@require_auth
def submit_post():
    data = json.loads(request.data)
    title = data.get("title")

    if not title:
        return respond(ok=False, data="post title cannot be blank"), 400

    category_id = data.get("category") or None
    post_id = data.get("post_id") or None
    content = remove_script_tags(data.get("content") or str())
    draft = data.get("draft") or False

    try:
        user = User.query.filter(User.user_id == session["user_id"]).one()
    except NoResultFound:
        return respond(ok=False, data="no such user"), 403

    if post_id:
        # Editing post
        try:
            post = Post.query.filter(Post.entry_id == post_id).one()
        except NoResultFound:
            return respond(ok=False, data="no such post"), 400

        post.title = title
        post.content = content
        post.is_draft = draft
        post.edited_now()
        db.session.add(post)
    else:
        # New post
        if not Category.find(category_id):
            return respond(ok=False, data="invalid category"), 400

        if Category.find(category_id).is_blog_category and not user.is_dev:
            return respond(ok=False, data="insufficient privileges"), 403

        post = Post(category_id, title, content, session["user_id"])
        post.created_now()
        post.is_draft = draft
        db.session.add(post)

    db.session.commit()
    url = post.url
    return respond(data=url)


@submissions.route("/reply", methods=["POST"])
@require_auth
def submit_reply():
    data = json.loads(request.data)
    reply_id = data.get("reply_id")
    post_id = data.get("post_id")
    content = remove_script_tags(data.get("content"))

    if not content:
        return respond(ok=False, data="missing content"), 400

    if not reply_id and not post_id:
        return respond(ok=False, data="missing post id"), 400

    try:
        user = User.query.filter(User.user_id == session["user_id"]).one()
    except NoResultFound:
        return respond(ok=False, data="no such user"), 403

    if reply_id:
        # Editing reply
        try:
            reply = Reply.query.filter(Reply.entry_id == reply_id).one()
        except NoResultFound:
            return respond(ok=False, data="reply not found"), 400

        if user.user_id != reply.user_id:
            return respond(ok=False, data="insufficient access"), 403

        reply.content = content
        reply.edited_now()
        db.session.add(reply)
    else:
        # New reply
        try:
            Post.query.filter(Post.entry_id == post_id).one()
        except NoResultFound:
            return respond(ok=False, data="post not found"), 400

        reply = Reply(content, post_id, session["user_id"])
        reply.created_now()
        db.session.add(reply)

    db.session.commit()
    url = reply.url

    if not reply_id:
        # Send new reply notifications to post watchers
        notify_watchers(reply)

    return respond(data=url)
