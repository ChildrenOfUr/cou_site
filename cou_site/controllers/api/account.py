from flask import Blueprint, request, session

from cou_site.controllers.api import remove_script_tags, respond, require_auth
from cou_site.models import db
from cou_site.models.user import User

account = Blueprint("account", __name__, url_prefix="/api/account")


@account.route("/bio", methods=["POST"])
@require_auth
def _set_bio():
    if request.content_length > 100000:
        return respond(ok=False, data="bio is larger than 100 KB"), 413

    user = User.query.filter(User.user_id == session["user_id"]).one()
    new_bio = remove_script_tags(request.get_data(as_text=True))
    user.bio = new_bio
    db.session.commit()
    return respond(), 200


@account.route("/color", methods=["POST"])
@require_auth
def _set_color():
    if request.content_length != 6:
        return respond(ok=False, data="incorrect color string length"), 413

    color = request.get_data(as_text=True)

    try:
        int(color, base=16)
    except ValueError:
        return respond(ok=False, data="invalid color string"), 413

    user = User.query.filter(User.user_id == session["user_id"]).one()
    user.username_color = "#" + color
    db.session.commit()
    return respond(), 200
