import json

from flask import Blueprint, request, session, redirect

from cou_site.config import game_server_uri
from cou_site.controllers.api import respond
from cou_site.models.user import User

AUTH_SERVER = f"{game_server_uri}:8383/auth"
auth = Blueprint("auth", __name__, url_prefix="/api/auth")


@auth.route("/login", methods=["POST"])
def login_api():
    if not request.data:
        return respond(False, "missing login data"), 400

    session["auth"] = json.loads(request.data)
    session["user_id"] = (
        User.query.filter(User.email == session["auth"]["playerEmail"]).one()
    ).user_id
    return respond()


@auth.route("/logout")
def logout_api():
    session.pop("auth")
    session.pop("user_id")
    return redirect(request.referrer)
