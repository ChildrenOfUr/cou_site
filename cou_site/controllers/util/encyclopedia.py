def find_street_by_name(name: str, streets: dict):
    for street in streets.values():
        if street.name == name:
            return street
    return None
