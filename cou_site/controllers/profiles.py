from json import loads
from urllib.parse import quote_plus
from urllib.request import urlopen

from flask import Blueprint, render_template, abort, redirect
from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

from cou_site.config import game_server_uri
from cou_site.models import db
from cou_site.models.post import Post
from cou_site.models.reply import Reply
from cou_site.models.user import User
from .util import tsid_l

FORUM_LIST_LIMIT = 10

profiles = Blueprint("profiles", __name__, url_prefix="/players")

_progress_colors = ["danger", "warning", "info", "success"]

skill_categories = [
    "Alchemy",
    "Animals",
    "Cooking",
    "Gathering",
    "Growing",
    "Industrial",
    "Intellectual",
]

username_colors = {
    0: [
        ["#00008B", "Deep Underwater"],
        ["#006400", "Bureaucrat"],
        ["#8B0000", "Piggy Nibble"],
    ],
    10: [
        ["#E60073", "Saturated Piggy Pink"],
        ["#DFAF20", "Very Very Stinky Cheese"],
        ["#8B008B", "Juju Bandit"],
    ],
    20: [
        ["#6A5ACD", "Helga's Dress"],
        ["#FF7F50", "Conch Shell"],
        ["#737373", "Phantom"],
    ],
    30: [
        ["#48BEEF", "Teleporter"],
        ["#008B8B", "Stagnant Stream"],
        ["#000000", "No-No Hangover"],
    ],
    40: [
        ["#D2691E", "Rube"],
        ["#1E90FF", '"So blue I can\'t even describe it" —OMG BACON!!'],
        ["#FA7272", "Pocket Salmen"],
    ],
    50: [
        ["#8A2BE2", "Scion of Purple"],
        ["#FF69B4", "Flamingo Flamingo"],
        ["#32CD32", "Been Grean"],
    ],
    60: [
        ["#4D004B", "Wine of the Dead"],
        ["#00E8E8", "Ilmenskie Sky"],
        ["#009966", "Nyanite Necklace"],
    ],
}


@profiles.route("/")
def _player_index():
    return render_template("players/index.html", online_users=list_online_users())


@profiles.route("/<username>")
def _player(username: str):
    if not username:
        return abort(404)

    try:
        # Search without case sensitivity
        user = User.query.filter(func.lower(User.username) == username.lower()).one()
    except MultipleResultsFound:
        # Search with case sensitivity
        user = User.query.filter(User.username == username).one()
    except NoResultFound:
        return abort(404)

    if user.username != username:
        # Capitalization was incorrect
        return redirect(user.profile_url, code=301)

    posts = (
        Post.query.order_by(Post.created.desc())
        .filter(Post.user_id == user.user_id)
        .limit(FORUM_LIST_LIMIT)
        .all()
    )
    replies = (
        Reply.query.order_by(Reply.created.desc())
        .filter(Reply.user_id == user.user_id)
        .limit(FORUM_LIST_LIMIT)
        .all()
    )

    return render_template(
        "players/detail.html",
        user=user,
        metabolics=user.metabolics,
        progress_color=progress_color,
        street=get_street(user.metabolics.current_street) if user.metabolics else None,
        tsid_l=tsid_l,
        online=user.username in list_online_users(),
        achievements=get_achievements(user.username),
        skills=get_skills(user.username),
        username_colors=username_colors,
        forum_posts=posts,
        forum_replies=replies,
    )


def get_achievements(username: str) -> dict:
    """
    Returns a dictionary mapping achievement category string to a
    list of dicts representing achievements for the given player.
    """
    data = loads(
        urlopen(
            game_server_uri
            + "/listAchievements?excludeNonMatches=false&username="
            + quote_plus(username)
        ).read()
    )

    player_achv = dict()
    player_achv["_Awarded"] = list()

    for achv_id in data.keys():
        achv = data[achv_id]
        cat = achv["category"]
        if cat not in player_achv:
            player_achv[cat] = list()

        player_achv[cat].append(achv)

        if achv["awarded"] == "true":
            player_achv["_Awarded"].append(achv)

    return player_achv


def get_skills(username: str) -> dict:
    """
    Returns a dictionary mapping skill category string to a list of dicts
    representing skills for the given player.
    """
    data = loads(
        urlopen(
            game_server_uri + "/skills/getByUsername/" + quote_plus(username)
        ).read()
    )

    player_skills = dict()

    for skill in data:
        cat = skill["category"]
        if cat not in player_skills:
            player_skills[cat] = list()

        player_skills[cat].append(skill)

    return player_skills


def list_online_users() -> list:
    """Return a list of users currently playing the game."""
    json = urlopen(game_server_uri + "/serverStatus").read()
    return loads(json)["playerList"]


def get_street(tsid: str) -> str:
    """Find the street with the given TSID."""
    json = urlopen(game_server_uri + "/getStreet?tsid=" + tsid).read()
    return loads(json)


def progress_color(percent, reverse: bool = False) -> str or None:
    """Get the color a progress bar should be for a given value."""
    if percent <= 25:
        return _progress_colors[3] if reverse else _progress_colors[0]

    if percent <= 50:
        return _progress_colors[2] if reverse else _progress_colors[1]

    if percent <= 75:
        return _progress_colors[1] if reverse else _progress_colors[2]

    if percent <= 100:
        return _progress_colors[0] if reverse else _progress_colors[3]

    return None
