from flask import Blueprint, render_template, request, abort

from cou_site.config import game_server_uri as server, rs_token
from cou_site.controllers.encyclopedia.cache import (
    CategorizedCachedJSONResource,
    CachedEntitiesResource,
    CachedLocationsResource,
    CachedConnectingStreetsList,
    CachedJSONResource,
)
from cou_site.controllers.encyclopedia.search import search_data
from cou_site.models.encyclopedia.achievement import (
    Achievement,
    CATEGORIES as ACHIEVEMENT_CATEGORIES,
    PATH as ACHIEVEMENT_PATH,
)
from cou_site.models.encyclopedia.giants import GIANTS, PATH as GIANT_PATH
from cou_site.models.encyclopedia.item import (
    Item,
    CATEGORIES as ITEM_CATEGORIES,
    PATH as ITEM_PATH,
)
from cou_site.models.encyclopedia.location import PATH as LOCATION_PATH
from cou_site.models.encyclopedia.skill import (
    Skill,
    CATEGORIES as SKILL_CATEGORIES,
    PATH as SKILL_PATH,
)
from cou_site.controllers.util import split_camel_case

encyclopedia = Blueprint("encyclopedia", __name__, url_prefix="/encyclopedia")

entities = CachedEntitiesResource(f"{server}/entities/list?token={rs_token}")
skills = CategorizedCachedJSONResource(
    url=f"{server}/skills/list?token={rs_token}",
    dict_parser=Skill.from_dict,
    categories=SKILL_CATEGORIES,
)
achievements = CategorizedCachedJSONResource(
    url=f"{server}/listAchievements?token={rs_token}",
    dict_parser=Achievement.from_dict,
    categories=ACHIEVEMENT_CATEGORIES,
)
locations = CachedLocationsResource(f"{server}/getMapData?token={rs_token}")
items = CategorizedCachedJSONResource(
    url=f"{server}/getItems", dict_parser=Item.from_dict, categories=ITEM_CATEGORIES
)
world_graph = CachedJSONResource(f"file://worldGraph.json")


@encyclopedia.context_processor
def _inject_context():
    def sanitize_category(category_name: str) -> str:
        return category_name.replace(" ", "").replace("&", "").lower()

    return dict(
        # For example: locations
        current_section=request.path.split("/")[2],
        sanitize_category=sanitize_category,
    )


@encyclopedia.route("/")
def _index():
    return render_template("encyclopedia/index.html")


@encyclopedia.route("/search")
def _search():
    query = request.args.get("q", str())

    if not query:
        return abort(400)

    results = {
        "Achievements": search_data(query, achievements.data.by_id.values()),
        "Entities": search_data(query, entities.data.values()),
        "Items": search_data(query, items.data.by_id.values()),
        "Locations": search_data(
            query,
            set(locations.data.streets.values()) | set(locations.data.hubs.values()),
        ),
        "Giants": search_data(query, GIANTS.values()),
        "Skills": search_data(query, skills.data.by_id.values()),
    }

    have_results = False
    for result_list in results.values():
        if result_list:
            have_results = True
            break

    return render_template(
        "encyclopedia/search.html",
        query=query,
        results=results,
        have_results=have_results,
    )


@encyclopedia.route(f"/{ACHIEVEMENT_PATH}/")
def _achv_index():
    return render_template(
        "encyclopedia/categorical_list.html",
        list_title="Achievements",
        list_objs=achievements.data.by_cat,
    )


@encyclopedia.route(f"/{ACHIEVEMENT_PATH}/<achv_id>")
def _achv(achv_id: str):
    achievements_by_id = achievements.data.by_id

    if achv_id not in achievements_by_id:
        return abort(404)

    return render_template(
        "encyclopedia/categorical_object.html",
        path=ACHIEVEMENT_PATH,
        obj=achievements_by_id[achv_id],
    )


@encyclopedia.route("/entities/")
def _entities_index():
    return render_template(
        "encyclopedia/list.html",
        list_title="Entities",
        list_objs=entities.data.values(),
    )


@encyclopedia.route(f"/{ITEM_PATH}/")
def _items_index():
    return render_template(
        "encyclopedia/categorical_list.html",
        list_title="Items",
        list_objs=items.data.by_cat,
    )


@encyclopedia.route(f"/{ITEM_PATH}/<item_id>")
def _item(item_id: str):
    items_by_id = items.data.by_id

    if item_id not in items_by_id:
        return abort(404)

    return render_template(
        "encyclopedia/categorical_object.html",
        path=ITEM_PATH,
        obj=items_by_id[item_id],
        item=True,
    )


@encyclopedia.route(f"/{LOCATION_PATH}/")
def _locations_index():
    return render_template(
        "encyclopedia/locations.html",
        hubs=locations.data.hubs.values(),
        streets=locations.data.streets.values(),
    )


@encyclopedia.route(f"/{LOCATION_PATH}/<loc_id>")
def _location(loc_id: str):
    hubs, streets = locations.data

    if loc_id in hubs.keys():
        hub = hubs[loc_id]
        return render_template("encyclopedia/hub.html", hub=hub)
    elif loc_id in streets.keys():
        street = streets[loc_id]

        if not street.connections:
            street.connections = CachedConnectingStreetsList(
                street, locations, world_graph
            )

        return render_template(
            "encyclopedia/street.html",
            street=street,
            hub=hubs[street.category],
            split_camel_case=split_camel_case,
        )
    else:
        return abort(404)


@encyclopedia.route(f"/{GIANT_PATH}/")
def _giants_index():
    return render_template(
        "encyclopedia/list.html", list_title="Giants", list_objs=GIANTS.values()
    )


@encyclopedia.route(f"/{GIANT_PATH}/<giant_id>")
def _giant(giant_id: str):
    if giant_id not in GIANTS:
        return abort(404)

    return render_template("encyclopedia/giant.html", giant=GIANTS[giant_id])


@encyclopedia.route(f"/{SKILL_PATH}/")
def _skills_index():
    return render_template(
        "encyclopedia/categorical_list.html",
        list_title="Skills",
        list_objs=skills.data.by_cat,
    )


@encyclopedia.route(f"/{SKILL_PATH}/<skill_id>")
def _skill(skill_id: str):
    skills_by_id = skills.data.by_id

    if skill_id not in skills_by_id:
        return abort(404)

    return render_template(
        "encyclopedia/skill.html", skill=skills_by_id[skill_id], giants=GIANTS
    )
