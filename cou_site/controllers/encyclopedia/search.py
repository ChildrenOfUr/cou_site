from collections import namedtuple


SearchResult = namedtuple("SearchResult", "obj weight")


def search_data(query: str, dataset: set) -> list:
    results = set()

    for obj in dataset:
        # A set containing the string values of all the object's properties
        fields = {str(field) for field in vars(obj).values()}

        # Count the number of times the query appears in the object
        weight = " ".join(fields).lower().count(query.lower())

        # Keep objects containing the query string at least once
        if weight > 0:
            results.add(SearchResult(obj, weight))

    # Sort the results by relevance (most relevant first)
    results = [
        result.obj
        for result in sorted(results, key=lambda result: result.weight, reverse=True)
    ]

    # Limit the search to the top 10 results per dataset
    if len(results) > 10:
        results = results[:10]

    return results
