from math import ceil

from flask import Blueprint, render_template, abort, redirect, request
from sqlalchemy import and_, or_, func
from sqlalchemy.orm.exc import NoResultFound

from cou_site.controllers import edit_post as edit_generic_post
from cou_site.models.post import Post, PAGE as THREAD_PATH
from cou_site.models.reply import Reply
from cou_site.models.category import (
    Category,
    FORUM_CATEGORIES,
    PAGE as CATEGORY_PATH,
    POST_LIST_LIMIT,
)


forums = Blueprint("forums", __name__, url_prefix="/forums")


@forums.route("/")
def _index():
    return render_template("forums/index.html", categories=FORUM_CATEGORIES)


@forums.route(f"/{CATEGORY_PATH}/<cat_id>")
def category(cat_id):
    """Show the threads posted in a given category."""
    # Check category
    cat = Category.find(cat_id)
    if not cat:
        return abort(404)

    # Default page number
    page = request.args.get("page", "1")
    if not page:
        return redirect(cat.url, code=301)

    # Check page number
    if not str.isdigit(page):
        return abort(404)

    page = int(page)
    num_pages = max(ceil(cat.num_posts / POST_LIST_LIMIT), 1)

    # Page number out of bounds
    if page > num_pages:
        return abort(404)

    posts = (
        Post.query.order_by(Post.created.desc())
        .filter(Post.category_id == cat.cat_id)
        .limit(POST_LIST_LIMIT)
        .offset(POST_LIST_LIMIT * (page - 1))
        .all()
    )

    return render_template(
        "forums/category.html",
        category=cat,
        posts=posts,
        page=page,
        num_pages=num_pages,
    )


@forums.route(f"/{THREAD_PATH}/<post_id>")
def thread(post_id):
    """Show a post and its replies."""
    if not str.isdigit(post_id):
        return abort(404)

    try:
        post = Post.query.filter(Post.entry_id == post_id).one()
    except NoResultFound:
        return abort(404)

    if post.is_blog_post:
        return abort(404)

    return render_template("forums/thread.html", post=post)


@forums.route(f"/post/new")
def new_post():
    cat_id = request.args.get("category")
    if not cat_id:
        return abort(400)

    cat = Category.find(cat_id)
    if not cat or cat.is_blog_category:
        return abort(400)

    return render_template("edit.html", mode="post", entry=None, category=cat)


@forums.route(f"/edit/post")
def edit_post():
    post_id = request.args.get("id")

    if not post_id:
        return abort(400)

    return edit_generic_post(post_id)


@forums.route(f"/edit/reply")
def edit_reply():
    reply_id = request.args.get("id")

    if not reply_id:
        return abort(400)

    if not str.isdigit(reply_id):
        return abort(400)

    try:
        reply = Reply.query.filter(Reply.entry_id == reply_id).one()
    except NoResultFound:
        return abort(404)
    return render_template("edit.html", mode="reply", entry=reply)


@forums.route(f"/search")
def search():
    """Show the matching posts for a search query."""
    query = request.args.get("q", type=str, default=str()).lower()
    posts = []

    if query:
        posts = (
            Post.query.filter(
                and_(
                    Post.category_id != "comments",
                    or_(
                        func.lower(Post.title).contains(query),
                        func.lower(Post.content).contains(query),
                    ),
                )
            )
            .order_by(Post.created.desc())
            .all()
        )

    return render_template("forums/search.html", query=query, posts=posts)
