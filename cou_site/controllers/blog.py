from math import ceil

from flask import Blueprint, render_template, abort, session, request
from sqlalchemy import not_, and_
from sqlalchemy.orm.exc import NoResultFound

from cou_site.models.category import (
    Category,
    POST_LIST_LIMIT,
    PAGE as CAT_PATH,
    BLOG_CATEGORIES,
)
from cou_site.models.post import Post
from cou_site.models.user import User

blog = Blueprint("blog", __name__, url_prefix="/blog")
POST_PATH = "post"


@blog.route("/")
@blog.route(f"/{CAT_PATH}/<cat_id>")
def _index(cat_id=None):
    # Check category
    cat = Category.find(cat_id)
    if cat_id and not cat:
        return abort(404)

    # Check page number
    page = int(request.args.get("page", "1"))
    num_pages = max(ceil(cat.num_posts / POST_LIST_LIMIT), 1) if cat else 0

    # Page number out of bounds
    if page < 0 or (num_pages != 0 and page > num_pages):
        return abort(404)

    posts = Post.query.order_by(Post.created.desc()).filter(not_(Post.is_draft))
    if cat:
        posts = posts.filter(Post.category_id == cat_id)
    else:
        posts = posts.filter(Post.category_id.in_(c.cat_id for c in BLOG_CATEGORIES))

    num_posts = posts.count()
    posts = posts.limit(POST_LIST_LIMIT).offset(POST_LIST_LIMIT * (page - 1)).all()
    num_pages = max(ceil(num_posts / POST_LIST_LIMIT), 1)

    if "user_id" in session:
        try:
            user = User.query.filter(User.user_id == session["user_id"]).one()
        except NoResultFound:
            # Invalid user
            return abort(403)

        if user.is_dev:
            drafts = Post.query.filter(Post.is_draft).all()
        else:
            drafts = None
    else:
        user = None
        drafts = None

    return render_template(
        "blog/index.html",
        category=cat,
        posts=posts,
        page=page,
        num_pages=num_pages,
        is_dev=(user and user.is_dev),
        drafts=drafts,
        categories=BLOG_CATEGORIES,
    )


@blog.route(f"/{POST_PATH}/<post_id>")
def _post(post_id):
    try:
        post = Post.query.filter(Post.entry_id == post_id).one()
    except NoResultFound:
        return abort(404)

    if not post.is_blog_post:
        return abort(404)

    # Get previous post
    try:
        prev_post = (
            Post.query.filter(and_(Post.created < post.created, Post.is_blog_post))
            .order_by(Post.created.desc())
            .limit(1)
            .one()
        )
    except NoResultFound:
        prev_post = None

    # Get next post
    try:
        next_post = (
            Post.query.filter(and_(Post.created > post.created, Post.is_blog_post))
            .order_by(Post.created.asc())
            .limit(1)
            .one()
        )
    except NoResultFound:
        next_post = None

    if "user_id" in session:
        try:
            user = User.query.filter(User.user_id == session["user_id"]).one()
        except NoResultFound:
            # Invalid user
            return abort(403)
    else:
        user = None

    if post.is_draft and not (user and user.is_dev):
        return abort(403)

    return render_template(
        "blog/post.html",
        post=post,
        prev_post=prev_post,
        next_post=next_post,
        is_dev=(user and user.is_dev),
    )


@blog.route(f"/new")
def _new_post():
    cat_id = request.args.get("cat_id")
    cat = Category.find(cat_id)
    if not cat or not cat.is_blog_category:
        return abort(404)

    return render_template("edit.html", mode="post", entry=None, category=cat)


@blog.route(f"/edit")
def _edit_post():
    post_id = request.args.get("post")

    if not post_id:
        return abort(400)

    if not str.isdigit(post_id):
        return abort(404)

    try:
        post = Post.query.filter(Post.entry_id == post_id).one()
    except NoResultFound:
        return abort(404)

    valid_user = False

    if "user_id" in session:
        try:
            user = User.query.filter(User.user_id == session["user_id"]).one()
            valid_user = user.is_dev
        except NoResultFound:
            pass

    if not valid_user:
        return abort(403)

    return render_template("edit.html", mode="post", entry=post, category=post.category)
