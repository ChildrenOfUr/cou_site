from collections import namedtuple

from flask import Blueprint, redirect, render_template, abort

from cou_site.controllers.util.navigation import pages
from cou_site.models.user import User

team = Blueprint("team", __name__, url_prefix="/team")

TeamMember = namedtuple("TeamMember", "slug name role bio cou_username glitch_id")

developers = [
    TeamMember(
        slug="andy",
        name="Andy Castille",
        role="Tinkerer",
        bio=(
            "He joined the CoU project in early 2014 after watching "
            "the development process. He contributes code "
            "(like the recipes feature), helped write the forums software, "
            "and manage the website and profile pages."
        ),
        cou_username="Klikini",
        glitch_id="PHVRFUTFUQA2TAH",
    ),
    TeamMember(
        slug="robert",
        name="Robert McDermot",
        role="Regional Programming Liaison",
        cou_username="Thaderator",
        glitch_id="PUV8J99156F2N8M",
        bio=(
            'A coder monkey who brings a new meaning to the term "programmer art", '
            "Robert joined the project in the fall of 2013. "
            "A stout believer that Egyptian Brackets suck and has been known to "
            "use Pokemon Exception Handling one to many times. "
            '<a href="http://blog.codinghorror.com/new-programming-jargon/">'
            "Jargon Reference</a>"
        ),
    ),
    TeamMember(
        slug="develle",
        name="Laura Montgomery",
        role="Graphic Designer",
        bio=(
            "I'm here to help optimize graphics, missing assets, "
            "and help revive missing/broken/dead streets. Etc."
        ),
        glitch_id="PUV91K2AAKG2S00",
        cou_username="Elle Lament",
    ),
    TeamMember(
        slug="paul",
        name="Paul VanKeuren",
        role="Head Imagineer",
        bio=(
            "A philosophy student making games. "
            "He started the CoU Glitch revival project in 2013, "
            "shortly after the game closed and before the assets were released "
            "into the public domain. "
            "His contributions include design choices and code."
        ),
        cou_username="Paal",
        glitch_id="PIFSJMGTUI63Q6F",
    ),
    TeamMember(
        slug="courtney",
        name="Courtney B Reid",
        cou_username="Lead",
        role="Develop-o-nator and Designifier",
        bio=(
            "Joined the team in 2013, and has since added code, art, "
            "design, and good karma to the project. Her patron Giant is Lem."
        ),
        glitch_id="PHVBB509QK42GQD",
    ),
    TeamMember(
        slug="lizzard",
        name="Liz Henry",
        role="Dev",
        bio=None,
        cou_username="lizzard",
        glitch_id=None,
    ),
    TeamMember(
        slug="katharine",
        name="Katharine Gibeau",
        glitch_id="PIFPNVKDTL63AO3",
        role="Twitter Wizard",
        bio="Manages CoU’s social media platforms and communicates with the players "
        "and public. When stuff is happening at CoU, she will be sure to let you "
        "know about it.",
        cou_username=str(),
    ),
    TeamMember(
        slug="pixley",
        name="Craig Cottingham",
        glitch_id="PHFPPJFJTVC2RK0",
        bio=None,
        cou_username="Pixley",
        role=None,
    ),
]


def get_dev(slug: str) -> TeamMember or None:
    found = [m for m in developers if m.slug == slug]

    if found:
        return found[0]
    else:
        return None


@team.route("/")
def _index():
    return redirect(pages["about"].url + "#team")


@team.route("/<slug>")
def _developer(slug: str):
    dev = get_dev(slug)

    if not dev:
        return abort(404)

    return render_template(
        "team_member.html",
        member=dev,
        profile_url=f"{pages['profiles'].url}/{dev.cou_username}",
        avatar_url=User.make_avatar_url(dev.cou_username),
    )
