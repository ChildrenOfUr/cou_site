from datetime import datetime

from flask import Flask, session, render_template, url_for, redirect
from flask_mail import Mail

from .config import secret_key, mail_server, mail_username, mail_password, db_uri
from .controllers.util import plural_s, rel_time
from .controllers.util.backgrounds import rand_bg
from .controllers.util.navigation import pages, is_active_page
from .models import db

# Configure Flask
app = Flask(__name__)
app.secret_key = secret_key

# Configure email extension
app.config["mail_server"] = mail_server
app.config["mail_username"] = mail_username
app.config["mail_password"] = mail_password
mail = Mail(app)

# Configure database
app.config["SQLALCHEMY_DATABASE_URI"] = db_uri
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db.init_app(app)


@app.context_processor
def inject_global_context():
    return {"pages": pages.values(), "year": datetime.now().year}


@app.context_processor
def inject_util_funcs():
    return {
        "is_active_page": is_active_page,
        "plural_s": plural_s,
        "rand_bg": rand_bg,
        "rel_time": rel_time,
    }


@app.context_processor
def inject_login():
    data = {"logged_in": "auth" in session}

    if data["logged_in"]:
        data["logged_in_username"] = session["auth"]["playerName"]
        data["logged_in_user_id"] = session["user_id"]

    return data


@app.errorhandler(400)
@app.errorhandler(403)
@app.errorhandler(404)
@app.errorhandler(500)
def _error_page(e):
    return (
        render_template("error.html", error=e),
        e.code if hasattr(e, "code") else 500,
    )


@app.route("/packages/cou_login/cou_login/assets/logo.svg")
def cou_login_logo_request():
    """Intercept request for logo image from login widget."""
    return redirect(url_for("static", filename="img/logo/cou_ur.svg"), code=301)
