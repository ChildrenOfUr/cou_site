function sendLoginSession(data, callback) {
    $.ajax({
        type: "POST",
        url: "/api/auth/login",
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: callback,
        error: function (xhr, status, error) {
            alert("Could not log in: " + error);
        }
    });
}

let loginHandler = function (e) {
    // Only once
    document.removeEventListener("loginSuccess", loginHandler);

    // Set server data
    sendLoginSession(e.detail, function () {
        // Redirect back to login referrer,
        // or the homepage if the referrer
        // is the login page to prevent looping.
        let redirect = document.referrer;
        if (redirect === window.location.href) {
            redirect = "/";
        }
        window.location.replace(redirect);
    });
};

document.addEventListener("loginSuccess", loginHandler, false);
