from base64 import b64encode
from urllib.error import HTTPError
from urllib.request import urlopen

from flask import url_for

from .game_object import GameObject

PATH = "entities"

STATIC_ENTITY_IMAGES = "https://childrenofur.com/assets/staticEntityImages"


class Entity(GameObject):
    @staticmethod
    def from_dict(entity: dict):
        return Entity(
            obj_id=entity["id"],
            name=entity["name"],
            category=entity["category"],
            states=entity.get("states", list()),
            current_state=entity.get("currentState", str()),
            responses=entity.get("responses", list()),
            sell_items=entity.get("sellItems", list()),
        )

    def __init__(
        self,
        obj_id: str,
        name: str,
        category: str,
        states: list,
        current_state: str,
        responses: str,
        sell_items: list,
    ):
        # Determine icon URL
        if category == "Shrine":
            icon_url = url_for("static", filename="img/encyclopedia/shrine.png")
        elif "Vendor" in name or "Street Spirit" in name:
            icon_url = url_for("static", filename="img/currant.svg")
        else:
            # Check if there is a static entity image available
            try:
                with urlopen(f"{STATIC_ENTITY_IMAGES}/{name}.png") as image:
                    icon_url = b64encode(image.read())
            except HTTPError:
                icon_url = url_for("static", filename="img/encyclopedia/entities.png")

        super().__init__(obj_id, name, category, icon_url)
        self.states = states
        self.current_state = current_state
        self.responses = responses
        self.sell_items = sell_items

    @property
    def path(self) -> str:
        return PATH
