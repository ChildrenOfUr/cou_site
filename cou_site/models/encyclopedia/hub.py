from flask import url_for

from cou_site.controllers.encyclopedia.cache import CachedStreetsList
from .location import Location


class Hub(Location):
    @staticmethod
    def from_dict(hub_id: (str, int), hub: dict, locations):
        return Hub(
            hub_id=str(hub_id),
            label=hub["name"],
            music=hub.get("music", None),
            players_have_letters=hub.get("players_have_letters", False),
            disable_weather=hub.get("disable_weather", False),
            snowy_weather=hub.get("snowy_weather", False),
            physics=hub.get("physics", "normal"),
            in_game=hub.get("in_game", True),
            locations=locations,
        )

    def __init__(
        self,
        hub_id: str,
        label: str,
        music: str,
        players_have_letters: bool,
        disable_weather: bool,
        snowy_weather: bool,
        physics: str,
        in_game: bool,
        locations,
    ):
        img_url = url_for("static", filename=f"img/hubs/{label}.jpg")
        super().__init__(hub_id, label, None, img_url, in_game)
        self.music = music
        self.players_have_letters = players_have_letters
        self.disable_weather = disable_weather
        self.snowy_weather = snowy_weather
        self.physics = physics
        self._streets = CachedStreetsList(self, locations)

    @property
    def streets(self):
        return self._streets.data
