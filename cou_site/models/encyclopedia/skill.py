from .game_object import GameObject


PATH = "skills"

CATEGORIES = [
    "Alchemy",
    "Animals",
    "Gathering",
    "Growing",
    "Industrial",
    "Intellectual",
]


class Skill(GameObject):
    @staticmethod
    def from_dict(skill: dict):
        return Skill(
            obj_id=skill["id"],
            name=skill["name"],
            category=skill["category"],
            descriptions=skill["descriptions"],
            levels=skill["levels"],
            icon_urls=skill["iconUrls"],
            giants=skill["giants"],
        )

    def __init__(
        self,
        obj_id: str,
        name: str,
        category: str,
        descriptions: list,
        levels: list,
        icon_urls: list,
        giants: list,
    ):
        super().__init__(obj_id, name, category, icon_urls[-1])
        self.descriptions = descriptions
        self.levels = levels
        self.icon_urls = icon_urls
        self.giants = giants

    @property
    def path(self) -> str:
        return PATH
