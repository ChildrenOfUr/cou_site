from datetime import timedelta
from re import sub
from typing import Union

from flask import url_for

from cou_site.config import game_server_uri, rs_token
from cou_site.controllers.util import tsid_l, tsid_g
from cou_site.controllers.encyclopedia.cache import CachedJSONResource
from .giants import Giant, GIANTS
from .hub import Hub
from .location import Location


CAT422_LOCATIONS = (
    "https://raw.githubusercontent.com/ChildrenOfUr/"
    "CAT422-glitch-location-viewer/master/locations"
)
STREET_ICON = url_for("static", filename="img/encyclopedia/signpost.png")


class Street(Location):
    @staticmethod
    def from_dict(label: str, street: dict, locations):
        return Street(
            tsid=street["tsid"],
            label=label,
            hub_id=str(street["hub_id"]),
            is_hidden=street.get("map_hidden", False),
            is_broken=street.get("broken", False),
            has_mailbox=street.get("mailbox", False),
            vendor=street.get("vendor", None),
            shrine=street.get("shrine", None),
            in_game=street.get("in_game", True),
            locations=locations,
        )

    def __init__(
        self,
        tsid: str,
        label: str,
        hub_id: str,
        is_hidden: bool,
        is_broken: bool,
        has_mailbox: bool,
        vendor: str,
        shrine: str,
        in_game: bool,
        locations,
    ):
        super().__init__(tsid_l(tsid), label, hub_id, STREET_ICON, in_game)
        self.hidden = is_hidden
        self.broken = is_broken
        self.has_mailbox = has_mailbox
        self.vendor = vendor
        self.shrine = shrine
        self.cat422 = CachedJSONResource(
            f"{CAT422_LOCATIONS}/{tsid_g(tsid)}.json", max_age=timedelta(weeks=1)
        )
        self._entities = CachedJSONResource(
            f"{game_server_uri}/getEntities?tsid={tsid_g(tsid)}&token={rs_token}"
        )
        self.connections = None
        self._locations = locations

    def hub(self) -> Hub:
        return self._locations.data.hubs[self.category]

    @property
    def entities(self) -> dict:
        return self._entities.data["entities"]

    @property
    def entity_counts(self) -> dict:
        counts = dict()

        for entity in self.entities:
            # Convert the entity type from combined Snake_CamelCase to Space Case
            entity_type = sub(
                r"([a-z])([A-Z])", r"\g<1> \g<2>", entity["type"].replace("_", " ")
            )

            if entity_type not in counts:
                counts[entity_type] = 0

            counts[entity_type] += 1

        return counts

    @property
    def giant(self) -> Union[Giant, None]:
        if self.shrine:
            return GIANTS[self.shrine.lower()]
        else:
            return None
