from .game_object import GameObject


PATH = "achievements"

CATEGORIES = [
    "Alchemy",
    "Animals",
    "Cooking",
    "Cultivation",
    "Exploring",
    "Feats",
    "Furniture",
    "Games",
    "Gardens",
    "Giants",
    "Harvesting",
    "Industrial",
    "Player",
    "Projects",
    "Seasonal",
    "Social",
    "Trees",
    "Trophies",
]


class Achievement(GameObject):
    @staticmethod
    def from_dict(achv: dict):
        return Achievement(
            obj_id=achv["id"],
            name=achv["name"],
            category=achv["category"],
            icon_url=achv["imageUrl"],
            description=achv["description"],
        )

    def __init__(
        self, obj_id: str, name: str, category: str, icon_url: str, description: str
    ):
        super().__init__(obj_id, name, category, icon_url)
        self.description = description

    @property
    def path(self):
        return PATH
