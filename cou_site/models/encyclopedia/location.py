from typing import Union

from .game_object import GameObject


PATH = "locations"


class Location(GameObject):
    def __init__(
        self,
        obj_id: str,
        name: str,
        category: Union[str, None],
        icon_url: str,
        in_game: bool,
    ):
        super().__init__(obj_id, name, category, icon_url)
        self.in_game = in_game

    @property
    def path(self) -> str:
        return PATH
