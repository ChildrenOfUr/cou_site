from gevent.pywsgi import WSGIServer

from cou_site import app
from cou_site.config import port

from cou_site.controllers import core_pages
from cou_site.controllers.api import api
from cou_site.controllers.api.account import account
from cou_site.controllers.api.auth import auth
from cou_site.controllers.api.deletions import deletions
from cou_site.controllers.api.submissions import submissions
from cou_site.controllers.api.voting import voting
from cou_site.controllers.api.watching import watching
from cou_site.controllers.blog import blog
from cou_site.controllers.encyclopedia import encyclopedia
from cou_site.controllers.forums import forums
from cou_site.controllers.guides import guides
from cou_site.controllers.profiles import profiles
from cou_site.controllers.team import team


for bp in [
    account,
    api,
    auth,
    blog,
    core_pages,
    deletions,
    encyclopedia,
    forums,
    guides,
    profiles,
    submissions,
    team,
    voting,
    watching,
]:
    app.register_blueprint(bp)


if __name__ == "__main__":
    http_server = WSGIServer((str(), port), app)
    print(f"Web server running on port {http_server.server_port}.")
    http_server.serve_forever()
